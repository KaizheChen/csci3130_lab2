// src/App.js
import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Navbar from './components/Navbar';
import Home from './pages/Home';
import About from './pages/About';
import Contact from './pages/Contact';

const App = () => {
    const [visits, setVisits] = useState(0);

    return (
        <Router>
            <Navbar />
            <Routes>
                <Route exact path="/" element={<Home visits={visits} setVisits={setVisits} />} />
                <Route path="/about" element={<About visits={visits} />} />
                <Route path="/contact" element={<Contact visits={visits} />} />
            </Routes>
        </Router>
    );
};

export default App;

