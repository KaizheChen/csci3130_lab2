// src/pages/Home.js
import React, { useEffect, useRef } from 'react';

const Home = ({ visits, setVisits }) => {
    const hasVisited = useRef(false);

    useEffect(() => {
        if (!hasVisited.current) {
            setVisits(visits + 1);
            hasVisited.current = true;
        }
    }, [setVisits, visits]);

    return (
        <div>
            <h1>Welcome</h1>
            <p>Banner ID：B00821396</p>
        </div>
    );
};

export default Home;
