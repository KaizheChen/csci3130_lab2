import React from 'react';

const About = ({ visits }) => {
    return (
        <div>
            <h1>Welcome to About page</h1>
            <p>Home page visited {visits} times</p>
        </div>
    );
};

export default About;